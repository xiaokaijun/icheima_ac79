#include "system/includes.h"
#include "app_config.h"
#include "uart.h"


#if USE_ICHEIMA_UART_DEMO
/**
    1. 如何初始化
    2. 如何接收
    3. 如何发送

    1.移植lvgl_v8将之前的表盘显示出来
    2.将表盘时间显示为实时时间
         2.1 联网
         2.2 获取ntp服务器的时间 SDK
         2.3 将服务器的时间设置给RTC SDK
    3.根据RTC时间，去更新表盘数据
         3.1 获取RTC时间
         3.2 更新ui的内容
*/
static u8 buf[1 * 1024] __attribute__((aligned(32))); //用于串口接收缓存数据的循环buf

static void *hdl = NULL;
void icheima_uart_init(){
    // 打开设备
    hdl = dev_open("uart1", NULL);
    if (!hdl) {
        printf("open uart err !!!\n");
        return ;
    }
    /* 1 . 设置串口接收缓存数据的循环buf地址 */
    dev_ioctl(hdl, UART_SET_CIRCULAR_BUFF_ADDR, (int)buf);

    /* 1 . 设置串口接收缓存数据的循环buf长度 */
    dev_ioctl(hdl, UART_SET_CIRCULAR_BUFF_LENTH, sizeof(buf));

    /* 3 . 设置接收数据为阻塞方式,需要非阻塞可以去掉,建议加上超时设置 */
    dev_ioctl(hdl, UART_SET_RECV_BLOCK, 1);

    /* u32 parm = 1000; */
    /* dev_ioctl(hdl, UART_SET_RECV_TIMEOUT, (u32)parm); //超时设置 */

    /* 4 . 使能特殊串口,启动收发数据 */
    dev_ioctl(hdl, UART_START, 0);

    printf("icheima_uart_init running \n");
}

int icheima_uart_recv(uint8_t* recv_buf,int length){
    /* 5 . 接收数据 */
    int len = dev_read(hdl, recv_buf, length);
    if (len <= 0) {
        printf("\n  uart recv err len = %d\n", len);
        if (len == UART_CIRCULAR_BUFFER_WRITE_OVERLAY) {
            printf("\n UART_CIRCULAR_BUFFER_WRITE_OVERLAY err\n");
            dev_ioctl(hdl, UART_FLUSH, 0); //如果由于用户长期不取走接收的数据导致循环buf接收回卷覆盖,因此直接冲掉循环buf所有数据重新接收
        } else if (len == UART_RECV_TIMEOUT) {
            puts("UART_RECV_TIMEOUT...\r\n");
        }
    }
    printf("\n uart recv len = %d\n", len);
    return len;
}

void icheima_uart_send(uint8_t* send_buf,int length){
    dev_write(hdl, send_buf, length);
}

void icheima_uart_test_task(){
    //1. 初始化
    icheima_uart_init();
    uint8_t recv_buf[64];
    while(1){
        int len = icheima_uart_recv(recv_buf,64);
        if(len > 0){
            icheima_uart_send(recv_buf,len);
        }
    }
}


static int c_main1(void)
{
    os_task_create(icheima_uart_test_task, NULL, 10, 1000, 0, "icheima_uart_test_task");
    return 0;
}

late_initcall(c_main1);

#endif // USE_ICHEIMA_UART_DEMO

