#include "app_config.h"
#include <time.h>
#include <sys/time.h>
#if LV_USE_PERF_MONITOR || LV_USE_MEM_MONITOR
#include "widgets/lv_label.h"
#endif


void icheima_get_current_time(char* time_str){
    struct tm timeinfo;
    time_t timestamp;
    timestamp = time(NULL) + 28800;
    localtime_r(&timestamp, &timeinfo);
    strftime(time_str, 64, "%Y-%m-%d %H:%M:%S", &timeinfo);

    printf("current time:%s",time_str);
}

#if USE_ICHEIMA_LVGL_UI_DEMO

#include "lv_conf.h"
#include "system/includes.h"
#include "lvgl.h"
//#include "lv_demo_benchmark.h"
#include "lv_demo_widgets.h"
//#include "lv_example_freetype.h"
#include "lv_port_disp.h"
#include "lv_port_indev.h"
#include "lv_port_fs.h"
#include "smart_watch/ui.h"
#include "icheima.h"
/*
static char inputbuffer[20]={0};
static int input_index = -1;
static int current_state = 0;
// 正常模式
#define NORM_STATE 0
// 管理员模式
#define ADMIN_STATE 1
// 添加用户
#define ADD_USER_STATE 2
// 删除用户
#define DEL_USER_STATE 3


void clear_buffer(){
  memset(inputbuffer, '\0', sizeof(inputbuffer));
  input_index=-1;
}
extern struct TempLockPassword tempLockPassword;
void norm_function(char key){
  current_state = NORM_STATE;

  printf("===============norm state=================\n");

  if(key == '#'){
    // 判断上一个按键是否为*
    char pre = inputbuffer[input_index-1];
    if(input_index > 0 && pre=='*'){
      // 清空数组
      //inputbuffer[20]={0};
      printf("1 add user\n");
      printf("2 del user\n");
      printf("* exit\n");
      // 切换到状态变化
      clear_buffer();
      current_state = ADMIN_STATE;
    }else{
      //extern char temp_lock_password[10];
      
      char* temp_lock_password = tempLockPassword.password;

      inputbuffer[input_index] = '\0';
        // 比对密码是否成功
      if(strcmp(inputbuffer,"123456") == 0){
        printf("success\r\n");
        post_msg_play_flash_mp3("004.mp3", 50);
      }else if(temp_lock_password!=NULL && strlen(temp_lock_password)>3){
        if(strcmp(inputbuffer,temp_lock_password) == 0){

            extern void icheima_mqtt_publish(int type,char* userId,char* passwordId);
            icheima_mqtt_publish(2,tempLockPassword.userId,tempLockPassword.id);

            post_msg_play_flash_mp3("004.mp3", 50);
        }
        printf("cmd:%s   temp_lock_password:%s \n",inputbuffer,temp_lock_password);
      }
      printf("cmd:%s   temp_lock_password:%s \n",inputbuffer,temp_lock_password);
      printf("cmd:%s \n",inputbuffer);
      clear_buffer();
    }
    printf("btn_text:# \n");
  }else if(key =='*'){
    printf("btn_text:* \n");
  }
}

void admin_function(char key){
  current_state = ADMIN_STATE;
  printf("===============ADMIN_STATE=================\n");
  if(key == '1'){
    printf("enter ADD_USER_STATE\n");
    // 切换到状态变化
    clear_buffer();
    current_state = ADD_USER_STATE;

  }else if(key == '2'){
    printf("enter DEL_USER_STATE\n");
  }else if(key == '#'){
    printf("cmd:%s \n",inputbuffer);
  }else if(key == '*'){
    current_state = NORM_STATE;
    printf("enter normal state\n");
  }
}

void add_user_function(char key){
  current_state = ADD_USER_STATE;
  printf("===============ADD_USER_STATE=================\n");
  if(key == '1'){

  }else if(key == '*'){
    current_state = NORM_STATE;
    printf("enter normal state\n");
  }else if(key == '#'){

    for (int i = 0; i < input_index; i++)
    {
      printf("%c\n",inputbuffer[i]);
    }
    printf("cmd:%s === %d\n",inputbuffer,input_index);
    clear_buffer();
  }
}



void btnmatrix_event_cb(lv_event_t* e){

  int code = lv_event_get_code(e);
  // 表示的是btnmatrix
  lv_obj_t* target = lv_event_get_target(e);
    post_msg_play_flash_mp3("Volume.mp3", 50);
  // 获取单击的是哪个按钮
  uint16_t btn_index = lv_btnmatrix_get_selected_btn(target);
  char* btn_text = lv_btnmatrix_get_btn_text(target,btn_index);

  inputbuffer[++input_index] = *btn_text;
  switch(current_state){
    case NORM_STATE:
      norm_function(*btn_text);
      break;
    case ADMIN_STATE:
      admin_function(*btn_text);
      break;
    case ADD_USER_STATE:
      add_user_function(*btn_text);
      break;
  }

  //input_index++;
}

void demo_matrix(){
  // 注意:  末尾的空串不能省略,  前面staic不能省
  static const char *map[] = {"1","2", "3","\n", "4", "5", "6","\n","7", "8", "9","\n","*","0","#", ""};
  // 显示按钮矩阵
  //1. 获取当前屏幕
  lv_obj_t* screen = lv_scr_act();
  // 2. 创建要显示的对象
  lv_obj_t* btn_matrix = lv_btnmatrix_create(screen);


  lv_obj_set_width(btn_matrix,240);
  lv_obj_set_height(btn_matrix,260);
  // lv_obj_set_pos(btn_matrix,0,80);

  //lv_btnmatrix_set_btn_width(btn_matrix,80);
  // 3. 设置按钮矩阵的内容
  lv_btnmatrix_set_map(btn_matrix,map);

  // 给按钮矩阵添加单击事件
  lv_obj_add_event_cb(btn_matrix,btnmatrix_event_cb, LV_EVENT_VALUE_CHANGED,NULL);

  // 创建文本,显示当前蓝牙设备号
  lv_obj_t* label = lv_label_create(screen);

  const char *name_p = bt_get_local_name();

  lv_label_set_text(label,name_p);
  lv_obj_align(label,LV_ALIGN_BOTTOM_MID,0,0);
}


static void lvgl_fs_test(void)
{
    extern int storage_device_ready(void);
    while (!storage_device_ready()) {//等待sd文件系统挂载完成
        os_time_dly(5);
        puts("lvgl waitting sd on... ");
    }

#if 1
    lv_obj_t *obpng = lv_img_create(lv_scr_act());
    lv_img_set_src(obpng, CONFIG_ROOT_PATH"icon.png");
    lv_obj_align(obpng, LV_ALIGN_CENTER, 0, 0);
#else
    lv_obj_t *imgbtn = lv_imgbtn_create(lv_scr_act());
    lv_imgbtn_set_src(imgbtn, LV_IMGBTN_STATE_PRESSED, NULL, CONFIG_ROOT_PATH"press_icon.bin", NULL);
    lv_imgbtn_set_src(imgbtn, LV_IMGBTN_STATE_RELEASED, NULL, CONFIG_ROOT_PATH"release_icon.bin", NULL);
    lv_obj_align(imgbtn, LV_ALIGN_CENTER, 0, 0);
#endif
}
*/

static OS_MUTEX mutex;


static void lvgl_time_ui_update(){
    char time_str[64];
    struct tm timeinfo;
    time_t timestamp;

    char buffer[10];
    char temp="%02d   %02d"; // "%02d   %02d"

    while(1){
        if(current_screen == 0){
            timestamp = time(NULL) + 28800;
            localtime_r(&timestamp, &timeinfo);
            strftime(time_str, sizeof(time_str), "%Y-%m-%dT%H:%M:%S", &timeinfo);
            //printf("RTC TIME [%s]\n", time_str);

            /* timeinfo.tm_hour  timeinfo.tm_min timeinfo.tm_sec
                ui_time   lv_label_set_text(ui_time, "03   27");
                ui_sec    lv_img_set_angle(ui_sec, 800); // 3600   1.0*20/60*3600
                ui_min    lv_img_set_angle(ui_sec, 800);
                ui_hour   lv_img_set_angle(ui_sec, 800);

                static OS_MUTEX mutex;
                os_mutex_pend(&mutex, 0);
                os_mutex_post(&mutex);
                os_mutex_create(&mutex);
            */

            os_mutex_pend(&mutex, 0);

            //sprintf(buffer, temp, timeinfo.tm_hour, timeinfo.tm_min);
            //lv_label_set_text(ui_time, buffer);
            lv_label_set_text_fmt(ui_labeltime,"%02d   %02d",timeinfo.tm_hour, timeinfo.tm_min);

            lv_img_set_angle(ui_second, 1.0*timeinfo.tm_sec/60*3600);
            lv_img_set_angle(ui_minute, 1.0*timeinfo.tm_min/60*3600);
            lv_img_set_angle(ui_hour, 1.0*timeinfo.tm_hour/12*3600);

            os_mutex_post(&mutex);
        }

        os_time_dly(100); // CV大师
    }
}


static void lvgl_main_task(void *priv)
{
    lv_init();
    lv_port_disp_init();
    lv_port_indev_init();
    lv_port_fs_init();
    lv_png_init();

    // uncomment one of these demos
    //    lv_example_arc_1();
    /*lv_demo_benchmark();*/
    /* lv_example_freetype_1(); */
    //lv_demo_widgets();
    //lv_example_rlottie_1();//FIXME:有死机问题 void renderer::Layer::render(VPainter *painter, const VRle &inheritMask
    //lv_example_rlottie_2();
    /* lv_example_btn_1(); */
    /*lv_demo_keypad_encoder();*/
    /*lv_demo_music();          */
    /*lv_demo_stress();           */
    /*lvgl_fs_test();*/
    ui_init();
    //demo_matrix();

    os_mutex_create(&mutex);

    thread_fork("lvgl_time_ui_update", 10, 512, 0, NULL, lvgl_time_ui_update, NULL);



    while (1) {
        os_mutex_pend(&mutex, 0);

        u32 time_till_next = lv_timer_handler();

        if (LV_DISP_DEF_REFR_PERIOD > 1 && time_till_next >= 1000 / OS_TICKS_PER_SEC) {
            msleep(time_till_next);
        }

        os_mutex_post(&mutex);
    }
}

static int lvgl_main_task_init(void)
{
    puts("lvgl_main_task_init \n\n");
    //说明由于LVGL没有加延时让出CPU需要配置为最低优先级 高优先级不能长时间占用CPU不然LVGL运行卡顿
    return thread_fork("lvgl_main_task", 1, 8 * 1024, 0, 0, lvgl_main_task, NULL);
}
late_initcall(lvgl_main_task_init);


#endif


