
#include "system/includes.h"
#include "asm/port_waked_up.h"
#include "typedef.h"
#include "os/os_api.h"
#include "asm/iic.h"
#include "device/iic.h"
#include "device/device.h"
#include "lcd_config.h"
#include "app_config.h"
#include "system/includes.h"
#include "gpio.h"
#include "ui_api.h"
#include "touch_event.h"
#include "sys_common.h"

#if TCFG_TOUCH_CST816T_ENABLE
#define TEST 0

#if 1
#define log_info(x, ...)    printf("\n[touch]>" x " \n", ## __VA_ARGS__)
#else
#define log_info(...)
#endif

struct touch_hdl {
    u16 x;
    u16 y;
    u8 status;
    u8 fingers;
};
struct touch_hdl lvgl_touch_hdl;

static OS_SEM touch_sem;

void *get_touch_x_y(void)
{
    return &lvgl_touch_hdl;
}

void get_touch_x_y_status(u16 *x, u16 *y, u8 *status)
{
    *x = lvgl_touch_hdl.x;
    *y = lvgl_touch_hdl.y;
    *status = lvgl_touch_hdl.status;
}
extern int ui_touch_msg_post(struct touch_event *event);


//#define CST816T_ADDRESS 0x2A
#define CST816T_ADDRESS_W 0x2A
#define CST816T_ADDRESS_R 0x2B
// 无触摸
#define CST816T_STATE_RELEASE 0xFF
// 检测到触摸
#define CST816T_STATE_PRESS_DOWN 0x00
// 上滑
#define CST816T_STATE_SLIDE_UP 0x01
// 下滑
#define CST816T_STATE_SLIDE_DOWN 0x02
// 左滑
#define CST816T_STATE_SLIDE_LEFT 0x03
// 右滑
#define CST816T_STATE_SLIDE_RIGHT 0x04
// 单击
#define CST816T_STATE_SINGLE_CLICK 0x05
// 双击
#define CST816T_STATE_DOUBLE_CLICK 0x0B
// 长按
#define CST816T_STATE_LONG_PRESS 0x0C

static void *iic = NULL;

static u8 touch_status = 0;




static void get_FT6236_xy(u16 addr, u16 *x, u16 *y)
{
    u8 buf[4];
    for (u8 i = 0; i < 4; i++) {
        rdFT6236Reg((addr + i), &buf[i]);	//读取XY坐标值
    }
    *y = (((u16)(buf[2] & 0X0F) << 8) + buf[3]);
    *x = ((u16)(buf[0] & 0X0F) << 8) + buf[1];
}

static u8 tp_last_staus = ELM_EVENT_TOUCH_UP;
static int tp_down_cnt = 0;



void CST816T_SendByte(uint8_t regID,uint8_t* regDat)
{
	//I2C_write(0x15, Addr,Data, 1);
	 u8 ret = 1;

    dev_ioctl(iic, IIC_IOCTL_START, 0);
    if (dev_ioctl(iic, IIC_IOCTL_TX_WITH_START_BIT, CST816T_ADDRESS_W)) {
        ret = 0;
        log_info("iic write err!!! line : %d \n", __LINE__);
        goto exit;
    }
    delay(100);
    if (dev_ioctl(iic, IIC_IOCTL_TX, regID & 0xff)) {
        ret = 0;
        log_info("iic write err!!! line : %d \n", __LINE__);
        goto exit;
    }
    delay(100);
    if (dev_ioctl(iic, IIC_IOCTL_TX_WITH_STOP_BIT, *regDat)) {
        ret = 0;
        log_info("iic write err!!! line : %d \n", __LINE__);
        goto exit;
    }
    delay(100);

exit:
    dev_ioctl(iic, IIC_IOCTL_STOP, 0);
}

void CST816T_ReceiveByte(uint8_t regID,uint8_t* Data)
{
	//I2C_read(0x15, Addr,Data, 1);
	 u8 ret = 1;
    dev_ioctl(iic, IIC_IOCTL_START, 0);
    if (dev_ioctl(iic, IIC_IOCTL_TX_WITH_START_BIT, CST816T_ADDRESS_W)) {
        ret = 0;
        log_info("iic write err!!! line : %d \n", __LINE__);
        goto exit;
    }
    delay(100);
    if (dev_ioctl(iic, IIC_IOCTL_TX, regID & 0xff)) {
        ret = 0;;
        log_info("iic write err!!! line : %d \n", __LINE__);
        goto exit;
    }
    delay(100);
    if (dev_ioctl(iic, IIC_IOCTL_TX_WITH_START_BIT, CST816T_ADDRESS_R)) {
        ret = 0;
        goto exit;
    }
    delay(100);
    dev_ioctl(iic, IIC_IOCTL_RX_WITH_STOP_BIT, (u32)Data);
exit:
    dev_ioctl(iic, IIC_IOCTL_STOP, 0);

}

#define CST816T_TOUCH_W 240
#define CST816T_TOUCH_H 280

void CST816_GetAction(void)
{
	uint8_t data[6];
	uint16_t X=0,Y=0;


	CST816T_ReceiveByte(0x01,data);
	CST816T_ReceiveByte(0x03,data+1);
	CST816T_ReceiveByte(0x04,data+2);
	CST816T_ReceiveByte(0x05,data+3);
	CST816T_ReceiveByte(0x06,data+4);
	// 读手指的个数
	CST816T_ReceiveByte(0x02,data+5);


	lvgl_touch_hdl.x=(uint16_t)((data[1]&0x0F)<<8)|data[2];//(temp[0]&0X0F)<<4|
	lvgl_touch_hdl.y=(uint16_t)((data[3]&0x0F)<<8)|data[4];//(temp[2]&0X0F)<<4|

    /*
	if(X<CST816T_TOUCH_W&&Y<CST816T_TOUCH_H)
	{
		X_Axis=X;
		Y_Axis=Y;
	}*/
	// 手指的数量
	lvgl_touch_hdl.fingers = data[5];

    if(lvgl_touch_hdl.fingers > 0 && lvgl_touch_hdl.x>0 && lvgl_touch_hdl.y>0){
		lvgl_touch_hdl.status=1;
	}else{
		lvgl_touch_hdl.status=0;
	}


	//printf("Sta:%X,X:%d,Y:%d finger:%d \t\n",lvgl_touch_hdl.status,lvgl_touch_hdl.x,lvgl_touch_hdl.y,lvgl_touch_hdl.fingers);
}

static void cst816t_interrupt(void)
{
    os_sem_post(&touch_sem);
}


void CST816T_Init(void)
{
	uint8_t ChipID=0;
	uint8_t FwVersion=0;

	//CST816T_Reset( );//芯片上电初始化

    os_sem_create(&touch_sem, 0);

    extern const struct ui_devices_cfg ui_cfg_data;
    static const struct ui_lcd_platform_data *pdata;
    pdata = (struct ui_lcd_platform_data *)ui_cfg_data.private_data;





    gpio_direction_output(pdata->touch_reset_pin, 0);
    os_time_dly(80);
    gpio_direction_output(pdata->touch_reset_pin, 1);
    os_time_dly(100);


    iic = dev_open("iic0", NULL);

    os_time_dly(500);

    CST816T_ReceiveByte(0xa7,&ChipID);
	CST816T_ReceiveByte(0xa9,&FwVersion);
	printf("ChipID:%X\r\n",ChipID);
	printf("FwVersion:%d\r\n",FwVersion);



    //注册中断注意触摸用的事件0 屏幕TE用的事件1
    port_wakeup_reg(EVENT_IO_0, pdata->touch_int_pin, EDGE_NEGATIVE, cst816t_interrupt);


    struct touch_event t;
    u8 last_action = ELM_EVENT_TOUCH_UP;
    while (1) {
        os_sem_pend(&touch_sem, 0);

        CST816_GetAction();

        t.x = lvgl_touch_hdl.x;
        t.y = lvgl_touch_hdl.y;

        if(lvgl_touch_hdl.fingers > 0 && lvgl_touch_hdl.x>0 && lvgl_touch_hdl.y>0){
            if(last_action == ELM_EVENT_TOUCH_DOWN){
                t.action = ELM_EVENT_TOUCH_MOVE;
            }else{
                t.action = ELM_EVENT_TOUCH_DOWN;
            }
        }else{
            t.action = ELM_EVENT_TOUCH_UP;
        }
        last_action = t.action;

        //ui_touch_msg_post(&t);
    }
}

void CST816T_ClrBaseDatas(void)
{
	lvgl_touch_hdl.fingers = 0;
	lvgl_touch_hdl.status = CST816T_STATE_RELEASE;
	lvgl_touch_hdl.x = 0;
	lvgl_touch_hdl.y = 0;
}



uint8_t CST816T_is_pressed(void){

	//CST816_GetAction();

	if(lvgl_touch_hdl.fingers > 0 && lvgl_touch_hdl.x>0 && lvgl_touch_hdl.y>0){
		return 1;
	}else{
		return 0;
	}
}


void CST816T_get_xy(uint16_t* x,uint16_t* y){
	*x = lvgl_touch_hdl.x;
	*y = lvgl_touch_hdl.y;
}



static void cst816t_touch_test_task(void *priv)
{
    CST816T_Init();
}

void set_touch_enable(void)
{

}

void set_touch_disable(void)
{

}



static int cst816t_task_init(void)
{
    //return thread_fork("cst816t_touch_test_task", 29, 1024, 0, NULL, cst816t_touch_test_task, NULL);
    return os_task_create(cst816t_touch_test_task, NULL, 25, 1000, 0, "cst816t_touch_test_task");
}
late_initcall(cst816t_task_init);

#endif
