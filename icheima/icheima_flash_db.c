#include "cJSON.h"
#include "app_config.h"
#include "system/includes.h"
#include "fs/fs.h"
#include "asm/sfc_norflash_api.h"

#if USE_ICHEIMA_DB





//char temp_lock_password[10]={'\0'};

char* print_icheima_db();

typedef struct temporary_password{
    int id;
    int userId;
    char* deadline;
    int count;
    char* password;
};

typedef struct User{
    int id;
    int type; // 0, 普通用户  1 管理员
    char* password;
};

typedef struct Wifi{
    char* ssid;
    char* passworld;
};


typedef struct icheima_db{
    struct Wifi;
    struct User admin;
    struct User user;
    struct temporary_password temporary;
};


#define USER_FLASH_SPACE_PATH "mnt/sdfile/EXT_RESERVED/user"
static u32 user_get_flash_exif_addr(void)
{
    u32 addr;
    FILE *profile_fp = fopen(USER_FLASH_SPACE_PATH, "r");
    if (profile_fp == NULL) {
        puts("user_get_flash_addr ERROR!!!\r\n");
        return 0;
    }
    struct vfs_attr file_attr;
    fget_attrs(profile_fp, &file_attr);
    addr = sdfile_cpu_addr2flash_addr(file_attr.sclust);
    fclose(profile_fp);

    printf("user_get_flash_exif_addr = 0x%x, size = 0x%x \r\n", addr, file_attr.fsize);
    return addr;
}

void icheima_flash_db_init(){
    u32 flash_exif_addr = user_get_flash_exif_addr();
    char buf[800];
    memset(buf, 0, sizeof(buf));

    char* value = print_icheima_db();
    printf("icheima_db create:%s==================\r\n",value);
    norflash_write(NULL, value, strlen(value), flash_exif_addr);

    put_buf(buf, sizeof(buf));

    memset(buf, 0, sizeof(buf));
    norflash_read(NULL, value, sizeof(buf), flash_exif_addr);
    put_buf(buf, sizeof(buf));

    printf("icheima_db save:%s==================\r\n",buf);
}


char* print_icheima_db(){
     cJSON *monitor = cJSON_CreateObject();
     cJSON_AddStringToObject(monitor, "name", "zhangsan");

end:
    char* string = cJSON_Print(monitor);
    cJSON_Delete(monitor);
    return string;
}
#endif
