/*******************************************************************************
 * Size: 16 px
 * Bpp: 1
 * Opts: --bpp 1 --size 16 --font C:/Users/KAI/Desktop/weifang/smart_watch/assets/AlimamaShuHeiTi-Bold.ttf -o C:/Users/KAI/Desktop/weifang/smart_watch/assets\ui_font_Font1.c --format lvgl -r 0x20-0x7f --symbols 黑马程序员鸿蒙智能门锁开发板 --no-compress --no-prefilter
 ******************************************************************************/

#include "../ui.h"

#ifndef UI_FONT_FONT1
#define UI_FONT_FONT1 1
#endif

#if UI_FONT_FONT1

/*-----------------
 *    BITMAPS
 *----------------*/

/*Store the image of the glyphs*/
static LV_ATTRIBUTE_LARGE_CONST const uint8_t glyph_bitmap[] = {
    /* U+0020 " " */
    0x0,

    /* U+0021 "!" */
    0xff, 0xfc, 0x3c,

    /* U+0022 "\"" */
    0xde, 0xf7, 0xbd, 0x80,

    /* U+0023 "#" */
    0x33, 0x19, 0xbf, 0xff, 0xf3, 0x33, 0x13, 0xff,
    0xff, 0x66, 0x33, 0x19, 0x80,

    /* U+0024 "$" */
    0x10, 0x10, 0x7e, 0xfe, 0xd0, 0xd0, 0xf0, 0x7e,
    0x1f, 0x13, 0x13, 0xff, 0xfc, 0x10, 0x10,

    /* U+0025 "%" */
    0x71, 0xcf, 0x98, 0x9b, 0x9, 0xb0, 0xfe, 0x7,
    0xfe, 0xf, 0xf1, 0xb3, 0x1b, 0x33, 0x3f, 0x71,
    0xe0,

    /* U+0026 "&" */
    0x1c, 0xf, 0x83, 0x60, 0xd8, 0x1c, 0xe, 0x37,
    0xef, 0x9e, 0xe7, 0x3f, 0xe3, 0xdc,

    /* U+0027 "'" */
    0xff, 0xc0,

    /* U+0028 "(" */
    0x19, 0xdc, 0xee, 0x73, 0x9c, 0xe7, 0x38, 0xe7,
    0x18, 0x60,

    /* U+0029 ")" */
    0xc7, 0x1c, 0xe3, 0x1c, 0xe7, 0x39, 0xce, 0xe7,
    0x73, 0x0,

    /* U+002A "*" */
    0x18, 0xa1, 0xf8, 0xc3, 0xc5, 0x80,

    /* U+002B "+" */
    0x18, 0x18, 0x18, 0xff, 0xff, 0x18, 0x18, 0x18,

    /* U+002C "," */
    0x37, 0x66, 0x60,

    /* U+002D "-" */
    0xff, 0xc0,

    /* U+002E "." */
    0xfc,

    /* U+002F "/" */
    0xc, 0x31, 0x86, 0x18, 0xc3, 0xc, 0x61, 0x86,
    0x30, 0xc0,

    /* U+0030 "0" */
    0x3c, 0x7e, 0xe7, 0xe7, 0xe7, 0xe7, 0xe7, 0xe7,
    0xe7, 0x7e, 0x3c,

    /* U+0031 "1" */
    0x3f, 0xff, 0x73, 0x9c, 0xe7, 0x39, 0xce,

    /* U+0032 "2" */
    0x7c, 0xff, 0x8f, 0x7, 0x7, 0xe, 0x1c, 0x38,
    0x70, 0xff, 0xff,

    /* U+0033 "3" */
    0x7e, 0x7f, 0x7, 0x7, 0x3e, 0x3e, 0x7, 0x7,
    0x87, 0xfe, 0x7c,

    /* U+0034 "4" */
    0xe, 0xe, 0x1e, 0x36, 0x36, 0x66, 0x66, 0xff,
    0xff, 0x6, 0x6,

    /* U+0035 "5" */
    0x7e, 0x7e, 0x60, 0x7c, 0x7e, 0xf, 0x7, 0x7,
    0x8f, 0xfe, 0x7c,

    /* U+0036 "6" */
    0x1e, 0x7e, 0x70, 0xfe, 0xff, 0xe3, 0xe3, 0xe3,
    0xe3, 0x7e, 0x3c,

    /* U+0037 "7" */
    0xff, 0xff, 0x6, 0xe, 0x1c, 0x18, 0x38, 0x38,
    0x30, 0x70, 0x70,

    /* U+0038 "8" */
    0x7e, 0x7f, 0xb0, 0xd8, 0x67, 0xe3, 0xf3, 0x9f,
    0x87, 0xc3, 0xff, 0x8f, 0x80,

    /* U+0039 "9" */
    0x3c, 0x7e, 0xc7, 0xc7, 0xc7, 0xff, 0x7f, 0x7,
    0xe, 0x7e, 0x7c,

    /* U+003A ":" */
    0xfc, 0x0, 0x7, 0xe0,

    /* U+003B ";" */
    0x77, 0x0, 0x7, 0x66, 0xcc,

    /* U+003C "<" */
    0x0, 0x81, 0xc7, 0xef, 0x8e, 0x7, 0x3, 0xe0,
    0x3e, 0x7, 0x80, 0x40,

    /* U+003D "=" */
    0xff, 0xff, 0x0, 0x0, 0xff, 0xff,

    /* U+003E ">" */
    0x0, 0x70, 0x3e, 0x3, 0xe0, 0x78, 0x1c, 0x7c,
    0xf8, 0xe0, 0x40, 0x0,

    /* U+003F "?" */
    0xfd, 0xfc, 0x38, 0x70, 0xe3, 0x8e, 0x18, 0x0,
    0x70, 0xe0,

    /* U+0040 "@" */
    0xf, 0xc0, 0xff, 0x8e, 0x1c, 0xcf, 0xbe, 0xfd,
    0xee, 0xcf, 0x66, 0x7b, 0x33, 0xdf, 0xf7, 0x77,
    0x1c, 0x0, 0x7c, 0x1, 0xe0, 0x0,

    /* U+0041 "A" */
    0x1f, 0x3, 0xe0, 0x6e, 0xd, 0xc3, 0x98, 0x73,
    0x8f, 0xf3, 0xfe, 0x70, 0xee, 0x1f, 0x83, 0x80,

    /* U+0042 "B" */
    0xff, 0x7f, 0xf8, 0xfc, 0x7f, 0xe7, 0xfb, 0x8f,
    0xc7, 0xe3, 0xff, 0xff, 0xc0,

    /* U+0043 "C" */
    0x1f, 0x8f, 0xf7, 0x87, 0xc0, 0xe0, 0x38, 0xe,
    0x3, 0xc0, 0x78, 0x4f, 0xf1, 0xf8,

    /* U+0044 "D" */
    0xfe, 0x3f, 0xee, 0x3b, 0x87, 0xe1, 0xf8, 0x7e,
    0x1f, 0x87, 0xe3, 0xbf, 0xef, 0xe0,

    /* U+0045 "E" */
    0xff, 0xff, 0xe0, 0xe0, 0xfe, 0xfe, 0xe0, 0xe0,
    0xe0, 0xff, 0xff,

    /* U+0046 "F" */
    0xff, 0xff, 0xe0, 0xe0, 0xfe, 0xfe, 0xe0, 0xe0,
    0xe0, 0xe0, 0xe0,

    /* U+0047 "G" */
    0xf, 0x8f, 0xf7, 0x83, 0xc0, 0xe1, 0xf8, 0x7e,
    0xf, 0xc3, 0x78, 0xcf, 0xf1, 0xf8,

    /* U+0048 "H" */
    0xe1, 0xf8, 0x7e, 0x1f, 0x87, 0xff, 0xff, 0xfe,
    0x1f, 0x87, 0xe1, 0xf8, 0x7e, 0x1c,

    /* U+0049 "I" */
    0xff, 0xff, 0xff, 0xff, 0x80,

    /* U+004A "J" */
    0x39, 0xce, 0x73, 0x9c, 0xe7, 0x39, 0xce, 0x7f,
    0x70,

    /* U+004B "K" */
    0xe3, 0xb8, 0xce, 0x73, 0xb8, 0xfc, 0x3f, 0xe,
    0xe3, 0x9c, 0xe3, 0xb8, 0x6e, 0x1c,

    /* U+004C "L" */
    0xe1, 0xc3, 0x87, 0xe, 0x1c, 0x38, 0x70, 0xe1,
    0xff, 0xf8,

    /* U+004D "M" */
    0xf8, 0xff, 0xc7, 0xfe, 0x3f, 0xfb, 0xfe, 0xdb,
    0xf6, 0xdf, 0xb6, 0xfd, 0xb7, 0xef, 0xbf, 0x39,
    0xf9, 0xce,

    /* U+004E "N" */
    0xf9, 0xfe, 0x7f, 0x9f, 0xe7, 0xed, 0xfb, 0x7e,
    0xdf, 0xb7, 0xe7, 0xf9, 0xfe, 0x7c,

    /* U+004F "O" */
    0x1f, 0xf, 0xf9, 0xc7, 0x70, 0x7e, 0xf, 0xc1,
    0xf8, 0x3f, 0x7, 0x71, 0xcf, 0xf8, 0x7c, 0x0,

    /* U+0050 "P" */
    0xfe, 0x7f, 0xb8, 0xfc, 0x7e, 0x3f, 0xff, 0xfd,
    0xc0, 0xe0, 0x70, 0x38, 0x0,

    /* U+0051 "Q" */
    0x1f, 0x83, 0xfc, 0x70, 0xef, 0xf, 0xe0, 0x7e,
    0x7, 0xe0, 0x7e, 0x7, 0x70, 0xe3, 0xfc, 0x1f,
    0x80, 0x18,

    /* U+0052 "R" */
    0xff, 0x7f, 0xf8, 0x7c, 0x3f, 0xff, 0xfb, 0x99,
    0xce, 0xe3, 0x71, 0xf8, 0x60,

    /* U+0053 "S" */
    0x3e, 0xfe, 0xe0, 0xe0, 0xf8, 0x3e, 0xf, 0x7,
    0x87, 0xff, 0x7c,

    /* U+0054 "T" */
    0xff, 0xff, 0x1c, 0x1c, 0x1c, 0x1c, 0x1c, 0x1c,
    0x1c, 0x1c, 0x1c,

    /* U+0055 "U" */
    0xe1, 0xf8, 0x7e, 0x1f, 0x87, 0xe1, 0xf8, 0x7e,
    0x1f, 0x87, 0xf3, 0xdf, 0xe3, 0xf0,

    /* U+0056 "V" */
    0xe0, 0xec, 0x1d, 0xc3, 0xb8, 0xe3, 0x1c, 0x73,
    0x8e, 0x60, 0xdc, 0x1b, 0x83, 0xe0, 0x7c, 0x0,

    /* U+0057 "W" */
    0xe3, 0x8e, 0xc7, 0x19, 0x8e, 0x73, 0x9c, 0xe7,
    0x6d, 0xc6, 0xdb, 0xd, 0xb6, 0x1b, 0x7c, 0x3e,
    0xf8, 0x38, 0xe0, 0x71, 0xc0,

    /* U+0058 "X" */
    0x70, 0xee, 0x38, 0xe7, 0xf, 0xc1, 0xf0, 0x1e,
    0x3, 0xc0, 0xfc, 0x39, 0xce, 0x39, 0xc3, 0x80,

    /* U+0059 "Y" */
    0x61, 0xce, 0x38, 0xee, 0x1d, 0xc1, 0xf0, 0x3e,
    0x3, 0x80, 0x70, 0xe, 0x1, 0xc0, 0x38, 0x0,

    /* U+005A "Z" */
    0xff, 0xff, 0xc1, 0xe0, 0xe0, 0xe0, 0xe0, 0xe0,
    0xe0, 0xf0, 0x7f, 0xff, 0xe0,

    /* U+005B "[" */
    0xff, 0xee, 0xee, 0xee, 0xee, 0xee, 0xef, 0xf0,

    /* U+005C "\\" */
    0xc0, 0x60, 0x60, 0x30, 0x30, 0x30, 0x18, 0x18,
    0xc, 0xc, 0xc, 0x6, 0x6, 0x3,

    /* U+005D "]" */
    0xff, 0xce, 0x73, 0x9c, 0xe7, 0x39, 0xce, 0x73,
    0xff, 0xe0,

    /* U+005E "^" */
    0x38, 0x71, 0xb3, 0x66, 0x78, 0xc0,

    /* U+005F "_" */
    0xff, 0xff,

    /* U+0060 "`" */
    0x63,

    /* U+0061 "a" */
    0x3b, 0xbf, 0xfc, 0xfc, 0x7e, 0x3f, 0x1f, 0xce,
    0xff, 0x3f, 0x80,

    /* U+0062 "b" */
    0xe0, 0x70, 0x38, 0x1c, 0xf, 0xe7, 0xfb, 0x9f,
    0xc7, 0xe3, 0xf1, 0xf9, 0xff, 0xef, 0xe0,

    /* U+0063 "c" */
    0x1f, 0x7f, 0xf0, 0xe0, 0xe0, 0xe0, 0xf1, 0x7f,
    0x1f,

    /* U+0064 "d" */
    0x3, 0x81, 0xc0, 0xe0, 0x73, 0xfb, 0xff, 0xcf,
    0xc7, 0xe3, 0xf1, 0xfc, 0xef, 0xf3, 0xf8,

    /* U+0065 "e" */
    0x3e, 0x3f, 0xb8, 0xdf, 0xff, 0xff, 0x3, 0x84,
    0xfe, 0x3f, 0x0,

    /* U+0066 "f" */
    0x3d, 0xf7, 0x1c, 0xff, 0xf7, 0x1c, 0x71, 0xc7,
    0x1c, 0x70,

    /* U+0067 "g" */
    0x3b, 0x7f, 0xf3, 0xe3, 0xe3, 0xe3, 0xf3, 0x7f,
    0x3f, 0x3, 0x87, 0xfe, 0xfc,

    /* U+0068 "h" */
    0xe0, 0xe0, 0xe0, 0xe0, 0xee, 0xff, 0xe7, 0xe7,
    0xe7, 0xe7, 0xe7, 0xe7, 0xe7,

    /* U+0069 "i" */
    0xfc, 0xf, 0xff, 0xff, 0xfe,

    /* U+006A "j" */
    0x39, 0xc0, 0x3, 0x9c, 0xe7, 0x39, 0xce, 0x73,
    0x9d, 0xfe, 0xe0,

    /* U+006B "k" */
    0xe0, 0xe0, 0xe0, 0xe0, 0xe7, 0xee, 0xec, 0xfc,
    0xfc, 0xee, 0xe6, 0xe7, 0xe3,

    /* U+006C "l" */
    0xee, 0xee, 0xee, 0xee, 0xee, 0xef, 0x70,

    /* U+006D "m" */
    0xee, 0xf7, 0xff, 0xf9, 0xcf, 0xce, 0x7e, 0x73,
    0xf3, 0x9f, 0x9c, 0xfc, 0xe7, 0xe7, 0x38,

    /* U+006E "n" */
    0xee, 0xff, 0xe7, 0xe7, 0xe7, 0xe7, 0xe7, 0xe7,
    0xe7,

    /* U+006F "o" */
    0x3e, 0x3f, 0xbd, 0xfc, 0x7e, 0x3f, 0x1f, 0xde,
    0xfe, 0x3e, 0x0,

    /* U+0070 "p" */
    0xee, 0x7f, 0xb9, 0xfc, 0x7e, 0x3f, 0x1f, 0x9f,
    0xfe, 0xfe, 0x70, 0x38, 0x1c, 0xe, 0x0,

    /* U+0071 "q" */
    0x3b, 0xbf, 0xfc, 0xfc, 0x7e, 0x3f, 0x1f, 0xce,
    0xff, 0x3f, 0x81, 0xc0, 0xe0, 0x70, 0x38,

    /* U+0072 "r" */
    0xff, 0xfe, 0x38, 0xe3, 0x8e, 0x38, 0xe0,

    /* U+0073 "s" */
    0x7d, 0xfb, 0x7, 0x87, 0xc1, 0xe3, 0xff, 0x7c,

    /* U+0074 "t" */
    0x73, 0xbf, 0xf7, 0x39, 0xce, 0x73, 0xce,

    /* U+0075 "u" */
    0xe7, 0xe7, 0xe7, 0xe7, 0xe7, 0xe7, 0xe7, 0xff,
    0x77,

    /* U+0076 "v" */
    0xe1, 0xb1, 0xd8, 0xee, 0x67, 0x31, 0xb8, 0xdc,
    0x7c, 0x1e, 0x0,

    /* U+0077 "w" */
    0xe3, 0x1d, 0x8e, 0x66, 0x79, 0x9d, 0xee, 0x37,
    0xb8, 0xda, 0xc3, 0xcf, 0xf, 0x3c, 0x1c, 0xe0,

    /* U+0078 "x" */
    0x63, 0xb9, 0x8f, 0xc3, 0xc1, 0xc0, 0xf0, 0xfc,
    0xe6, 0x63, 0x80,

    /* U+0079 "y" */
    0xe1, 0xb1, 0xdc, 0xe6, 0x63, 0x31, 0xf8, 0x7c,
    0x3c, 0x6, 0x7, 0x7, 0xf, 0x87, 0x0,

    /* U+007A "z" */
    0xff, 0xfc, 0x38, 0xe3, 0x8e, 0x38, 0x7f, 0xfe,

    /* U+007B "{" */
    0x3b, 0xdc, 0xe7, 0x39, 0xdc, 0xe3, 0x9c, 0xe7,
    0x3c, 0xe0,

    /* U+007C "|" */
    0xff, 0xff, 0xff, 0xfc,

    /* U+007D "}" */
    0xe7, 0x9c, 0xe7, 0x39, 0xc7, 0x3b, 0x9c, 0xe7,
    0x7b, 0x80,

    /* U+007E "~" */
    0xf0, 0xff, 0xf,

    /* U+53D1 "发" */
    0x33, 0x38, 0x6e, 0x31, 0xd8, 0x3, 0xff, 0xf7,
    0xff, 0xe1, 0x80, 0x3, 0xff, 0xf, 0xfe, 0x1b,
    0x18, 0x77, 0x70, 0xc7, 0xc1, 0x87, 0x7, 0x3f,
    0x8d, 0xf7, 0xfb, 0x3, 0x80,

    /* U+5458 "员" */
    0x3f, 0xf0, 0xff, 0xc3, 0x3, 0xf, 0xfc, 0x3f,
    0xf0, 0x0, 0x7, 0xff, 0x9f, 0xfe, 0x63, 0x19,
    0x8c, 0x66, 0x71, 0x9b, 0xc6, 0x1d, 0xe3, 0xe3,
    0xfe, 0x1, 0xc0,

    /* U+5E8F "序" */
    0x1, 0x81, 0xff, 0xf7, 0xff, 0xd8, 0x0, 0x6f,
    0xf9, 0xbf, 0xe6, 0x73, 0x18, 0xcc, 0x7f, 0xfd,
    0xff, 0xf6, 0xc, 0xd8, 0x33, 0x60, 0xc3, 0x9f,
    0xc, 0x7c, 0x0,

    /* U+5F00 "开" */
    0x7f, 0xfc, 0xff, 0xf8, 0x60, 0xc0, 0xc1, 0x81,
    0x83, 0x3, 0x6, 0x3f, 0xff, 0xff, 0xff, 0x18,
    0x30, 0x30, 0x60, 0x60, 0xc0, 0xc1, 0x83, 0x83,
    0x6, 0x6, 0x1c, 0xc, 0x0,

    /* U+667A "智" */
    0x7e, 0xff, 0xf7, 0xcc, 0x27, 0xfd, 0x3f, 0xe9,
    0xbe, 0x7f, 0xbb, 0xf0, 0x40, 0x7f, 0xfb, 0xff,
    0xd8, 0x6, 0xff, 0xf6, 0x1, 0xbf, 0xfc,

    /* U+677F "板" */
    0x19, 0xfe, 0x33, 0xff, 0xfe, 0x7, 0xfc, 0x1,
    0x98, 0xf, 0x3f, 0xdf, 0x7f, 0xbf, 0xf6, 0x5f,
    0xec, 0xb3, 0xf1, 0x66, 0xe6, 0xcd, 0xcd, 0xbb,
    0xdb, 0x6d, 0x86, 0xf9, 0x80,

    /* U+7A0B "程" */
    0xff, 0xfd, 0xff, 0xf8, 0xce, 0x31, 0x9c, 0x6f,
    0xff, 0xdf, 0xff, 0x8c, 0x0, 0x79, 0xfe, 0xff,
    0xfd, 0xf8, 0xc3, 0xdf, 0xf7, 0x9f, 0xeb, 0x6,
    0x16, 0x7f, 0xcc, 0xff, 0x80,

    /* U+80FD "能" */
    0x30, 0xc1, 0xf3, 0x36, 0x6f, 0xff, 0xbc, 0xfe,
    0xc0, 0x3, 0xff, 0xef, 0xff, 0x80, 0xc6, 0xc7,
    0xfb, 0xfc, 0x6f, 0xb1, 0xb8, 0xfe, 0xc3, 0x1b,
    0xfc, 0x6f, 0xc0,

    /* U+8499 "蒙" */
    0xff, 0xff, 0xff, 0xf1, 0x86, 0x3f, 0xff, 0xc0,
    0xf, 0x7f, 0xb0, 0x0, 0x3f, 0xff, 0xf, 0x1,
    0xff, 0x76, 0x3f, 0xbf, 0xfc, 0xf1, 0xbb, 0xfe,
    0x6f, 0x18, 0xc0,

    /* U+9501 "锁" */
    0x66, 0x6d, 0xfd, 0xb7, 0xb6, 0xb9, 0xff, 0xff,
    0xfd, 0xfc, 0x36, 0x76, 0xd9, 0xdb, 0xff, 0x6f,
    0xfd, 0xb6, 0x7e, 0xd8, 0x38, 0x7f, 0xbd, 0xfc,
    0x74, 0x40, 0x0,

    /* U+95E8 "门" */
    0x67, 0xfb, 0xbf, 0xcc, 0x6, 0x0, 0x3c, 0x1,
    0xe0, 0xf, 0x0, 0x78, 0x3, 0xc0, 0x1e, 0x0,
    0xf0, 0x7, 0x80, 0x3c, 0x1, 0xe0, 0x3f, 0x1,
    0xe0,

    /* U+9A6C "马" */
    0x7f, 0xf1, 0xff, 0xc3, 0x3, 0xc, 0xc, 0x30,
    0x30, 0xc0, 0xc3, 0xff, 0xcf, 0xff, 0x0, 0xc,
    0x0, 0x3f, 0xfe, 0xff, 0xfb, 0x0, 0xc, 0x1,
    0xf0, 0x7, 0xc0,

    /* U+9E3F "鸿" */
    0xc0, 0x33, 0xbf, 0xf6, 0xff, 0xc1, 0xb3, 0x86,
    0xff, 0x9b, 0xfe, 0x6f, 0xc1, 0xbf, 0x66, 0xfd,
    0x98, 0x36, 0xff, 0xdf, 0xff, 0xdc, 0xf, 0x0,
    0x7c, 0x1, 0xc0,

    /* U+9ED1 "黑" */
    0x7f, 0xfc, 0xff, 0xf9, 0xee, 0xf3, 0xff, 0xe6,
    0x7c, 0xcf, 0xff, 0x80, 0xe0, 0x3f, 0xfe, 0x3,
    0x81, 0xff, 0xff, 0xff, 0xf8, 0x0, 0x0, 0x0,
    0xc, 0xd9, 0xbb, 0xbb, 0x80
};


/*---------------------
 *  GLYPH DESCRIPTION
 *--------------------*/

static const lv_font_fmt_txt_glyph_dsc_t glyph_dsc[] = {
    {.bitmap_index = 0, .adv_w = 0, .box_w = 0, .box_h = 0, .ofs_x = 0, .ofs_y = 0} /* id = 0 reserved */,
    {.bitmap_index = 0, .adv_w = 67, .box_w = 1, .box_h = 1, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1, .adv_w = 95, .box_w = 2, .box_h = 11, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 4, .adv_w = 106, .box_w = 5, .box_h = 5, .ofs_x = 1, .ofs_y = 6},
    {.bitmap_index = 8, .adv_w = 154, .box_w = 9, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 21, .adv_w = 151, .box_w = 8, .box_h = 15, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 36, .adv_w = 213, .box_w = 12, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 53, .adv_w = 169, .box_w = 10, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 67, .adv_w = 59, .box_w = 2, .box_h = 5, .ofs_x = 1, .ofs_y = 6},
    {.bitmap_index = 69, .adv_w = 101, .box_w = 5, .box_h = 15, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 79, .adv_w = 101, .box_w = 5, .box_h = 15, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 89, .adv_w = 118, .box_w = 7, .box_h = 6, .ofs_x = 0, .ofs_y = 7},
    {.bitmap_index = 95, .adv_w = 148, .box_w = 8, .box_h = 8, .ofs_x = 1, .ofs_y = 1},
    {.bitmap_index = 103, .adv_w = 84, .box_w = 4, .box_h = 5, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 106, .adv_w = 113, .box_w = 5, .box_h = 2, .ofs_x = 1, .ofs_y = 4},
    {.bitmap_index = 108, .adv_w = 84, .box_w = 3, .box_h = 2, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 109, .adv_w = 128, .box_w = 6, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 119, .adv_w = 151, .box_w = 8, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 130, .adv_w = 151, .box_w = 5, .box_h = 11, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 137, .adv_w = 151, .box_w = 8, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 148, .adv_w = 151, .box_w = 8, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 159, .adv_w = 151, .box_w = 8, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 170, .adv_w = 151, .box_w = 8, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 181, .adv_w = 151, .box_w = 8, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 192, .adv_w = 151, .box_w = 8, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 203, .adv_w = 151, .box_w = 9, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 216, .adv_w = 151, .box_w = 8, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 227, .adv_w = 103, .box_w = 3, .box_h = 9, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 231, .adv_w = 103, .box_w = 4, .box_h = 10, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 236, .adv_w = 148, .box_w = 9, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 248, .adv_w = 148, .box_w = 8, .box_h = 6, .ofs_x = 1, .ofs_y = 2},
    {.bitmap_index = 254, .adv_w = 148, .box_w = 9, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 266, .adv_w = 115, .box_w = 7, .box_h = 11, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 276, .adv_w = 219, .box_w = 13, .box_h = 13, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 298, .adv_w = 186, .box_w = 11, .box_h = 11, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 314, .adv_w = 173, .box_w = 9, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 327, .adv_w = 175, .box_w = 10, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 341, .adv_w = 180, .box_w = 10, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 355, .adv_w = 149, .box_w = 8, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 366, .adv_w = 142, .box_w = 8, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 377, .adv_w = 180, .box_w = 10, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 391, .adv_w = 184, .box_w = 10, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 405, .adv_w = 74, .box_w = 3, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 410, .adv_w = 84, .box_w = 5, .box_h = 14, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 419, .adv_w = 172, .box_w = 10, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 433, .adv_w = 134, .box_w = 7, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 443, .adv_w = 227, .box_w = 13, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 461, .adv_w = 186, .box_w = 10, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 475, .adv_w = 203, .box_w = 11, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 491, .adv_w = 169, .box_w = 9, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 504, .adv_w = 205, .box_w = 12, .box_h = 12, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 522, .adv_w = 169, .box_w = 9, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 535, .adv_w = 145, .box_w = 8, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 546, .adv_w = 143, .box_w = 8, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 557, .adv_w = 188, .box_w = 10, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 571, .adv_w = 184, .box_w = 11, .box_h = 11, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 587, .adv_w = 239, .box_w = 15, .box_h = 11, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 608, .adv_w = 187, .box_w = 11, .box_h = 11, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 624, .adv_w = 166, .box_w = 11, .box_h = 11, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 640, .adv_w = 167, .box_w = 9, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 653, .adv_w = 85, .box_w = 4, .box_h = 15, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 661, .adv_w = 129, .box_w = 8, .box_h = 14, .ofs_x = 0, .ofs_y = -1},
    {.bitmap_index = 675, .adv_w = 85, .box_w = 5, .box_h = 15, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 685, .adv_w = 148, .box_w = 7, .box_h = 6, .ofs_x = 1, .ofs_y = 5},
    {.bitmap_index = 691, .adv_w = 128, .box_w = 8, .box_h = 2, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 693, .adv_w = 111, .box_w = 4, .box_h = 2, .ofs_x = 1, .ofs_y = 11},
    {.bitmap_index = 694, .adv_w = 158, .box_w = 9, .box_h = 9, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 705, .adv_w = 158, .box_w = 9, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 720, .adv_w = 136, .box_w = 8, .box_h = 9, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 729, .adv_w = 158, .box_w = 9, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 744, .adv_w = 155, .box_w = 9, .box_h = 9, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 755, .adv_w = 102, .box_w = 6, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 765, .adv_w = 157, .box_w = 8, .box_h = 13, .ofs_x = 1, .ofs_y = -4},
    {.bitmap_index = 778, .adv_w = 153, .box_w = 8, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 791, .adv_w = 73, .box_w = 3, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 796, .adv_w = 81, .box_w = 5, .box_h = 17, .ofs_x = 0, .ofs_y = -4},
    {.bitmap_index = 807, .adv_w = 141, .box_w = 8, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 820, .adv_w = 81, .box_w = 4, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 827, .adv_w = 227, .box_w = 13, .box_h = 9, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 842, .adv_w = 155, .box_w = 8, .box_h = 9, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 851, .adv_w = 161, .box_w = 9, .box_h = 9, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 862, .adv_w = 162, .box_w = 9, .box_h = 13, .ofs_x = 1, .ofs_y = -4},
    {.bitmap_index = 877, .adv_w = 163, .box_w = 9, .box_h = 13, .ofs_x = 1, .ofs_y = -4},
    {.bitmap_index = 892, .adv_w = 104, .box_w = 6, .box_h = 9, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 899, .adv_w = 123, .box_w = 7, .box_h = 9, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 907, .adv_w = 94, .box_w = 5, .box_h = 11, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 914, .adv_w = 155, .box_w = 8, .box_h = 9, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 923, .adv_w = 153, .box_w = 9, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 934, .adv_w = 225, .box_w = 14, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 950, .adv_w = 155, .box_w = 9, .box_h = 9, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 961, .adv_w = 149, .box_w = 9, .box_h = 13, .ofs_x = 0, .ofs_y = -4},
    {.bitmap_index = 976, .adv_w = 132, .box_w = 7, .box_h = 9, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 984, .adv_w = 89, .box_w = 5, .box_h = 15, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 994, .adv_w = 57, .box_w = 2, .box_h = 15, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 998, .adv_w = 89, .box_w = 5, .box_h = 15, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 1008, .adv_w = 148, .box_w = 8, .box_h = 3, .ofs_x = 1, .ofs_y = 4},
    {.bitmap_index = 1011, .adv_w = 256, .box_w = 15, .box_h = 15, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 1040, .adv_w = 256, .box_w = 14, .box_h = 15, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 1067, .adv_w = 256, .box_w = 14, .box_h = 15, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 1094, .adv_w = 256, .box_w = 15, .box_h = 15, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 1123, .adv_w = 256, .box_w = 13, .box_h = 14, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 1146, .adv_w = 256, .box_w = 15, .box_h = 15, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 1175, .adv_w = 256, .box_w = 15, .box_h = 15, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 1204, .adv_w = 256, .box_w = 14, .box_h = 15, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 1231, .adv_w = 256, .box_w = 14, .box_h = 15, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 1258, .adv_w = 256, .box_w = 14, .box_h = 15, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 1285, .adv_w = 256, .box_w = 13, .box_h = 15, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 1310, .adv_w = 256, .box_w = 14, .box_h = 15, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 1337, .adv_w = 256, .box_w = 14, .box_h = 15, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 1364, .adv_w = 256, .box_w = 15, .box_h = 15, .ofs_x = 0, .ofs_y = -2}
};

/*---------------------
 *  CHARACTER MAPPING
 *--------------------*/

static const uint16_t unicode_list_1[] = {
    0x0, 0x87, 0xabe, 0xb2f, 0x12a9, 0x13ae, 0x263a, 0x2d2c,
    0x30c8, 0x4130, 0x4217, 0x469b, 0x4a6e, 0x4b00
};

/*Collect the unicode lists and glyph_id offsets*/
static const lv_font_fmt_txt_cmap_t cmaps[] =
{
    {
        .range_start = 32, .range_length = 95, .glyph_id_start = 1,
        .unicode_list = NULL, .glyph_id_ofs_list = NULL, .list_length = 0, .type = LV_FONT_FMT_TXT_CMAP_FORMAT0_TINY
    },
    {
        .range_start = 21457, .range_length = 19201, .glyph_id_start = 96,
        .unicode_list = unicode_list_1, .glyph_id_ofs_list = NULL, .list_length = 14, .type = LV_FONT_FMT_TXT_CMAP_SPARSE_TINY
    }
};

/*-----------------
 *    KERNING
 *----------------*/


/*Pair left and right glyphs for kerning*/
static const uint8_t kern_pair_glyph_ids[] =
{
    7, 43,
    9, 75,
    13, 18,
    13, 24,
    13, 26,
    15, 18,
    15, 24,
    15, 26,
    18, 13,
    18, 15,
    18, 27,
    18, 28,
    24, 13,
    24, 15,
    24, 27,
    24, 28,
    27, 18,
    27, 24,
    28, 18,
    28, 24,
    34, 43,
    34, 53,
    34, 55,
    34, 56,
    34, 58,
    34, 66,
    34, 68,
    34, 70,
    34, 72,
    34, 80,
    34, 82,
    36, 43,
    37, 55,
    37, 56,
    37, 57,
    38, 43,
    39, 15,
    39, 34,
    39, 66,
    39, 68,
    39, 70,
    39, 72,
    39, 80,
    39, 82,
    40, 43,
    41, 43,
    42, 43,
    44, 43,
    44, 66,
    44, 68,
    44, 70,
    44, 72,
    44, 80,
    44, 82,
    45, 34,
    45, 43,
    45, 53,
    45, 55,
    45, 56,
    45, 58,
    46, 43,
    47, 43,
    48, 55,
    48, 56,
    48, 57,
    49, 15,
    49, 34,
    49, 57,
    50, 55,
    50, 56,
    50, 57,
    51, 43,
    51, 58,
    51, 66,
    51, 68,
    51, 70,
    51, 72,
    51, 80,
    51, 82,
    53, 15,
    53, 34,
    53, 66,
    53, 68,
    53, 70,
    53, 72,
    53, 78,
    53, 79,
    53, 80,
    53, 81,
    53, 82,
    53, 83,
    53, 84,
    53, 86,
    55, 15,
    55, 34,
    55, 66,
    55, 68,
    55, 70,
    55, 72,
    55, 78,
    55, 79,
    55, 80,
    55, 81,
    55, 82,
    55, 83,
    55, 84,
    55, 86,
    56, 15,
    56, 34,
    56, 66,
    56, 68,
    56, 70,
    56, 72,
    56, 78,
    56, 79,
    56, 80,
    56, 81,
    56, 82,
    56, 83,
    56, 84,
    56, 86,
    57, 15,
    57, 43,
    57, 66,
    57, 68,
    57, 70,
    57, 72,
    57, 80,
    57, 82,
    58, 15,
    58, 34,
    58, 66,
    58, 68,
    58, 69,
    58, 70,
    58, 72,
    58, 78,
    58, 79,
    58, 80,
    58, 81,
    58, 82,
    58, 83,
    58, 84,
    58, 85,
    58, 86,
    59, 43,
    60, 43,
    60, 75,
    61, 43,
    61, 75,
    66, 43,
    67, 55,
    67, 56,
    67, 58,
    67, 87,
    67, 88,
    67, 89,
    67, 90,
    68, 43,
    69, 43,
    70, 55,
    70, 56,
    70, 58,
    70, 87,
    70, 88,
    70, 89,
    70, 90,
    72, 43,
    76, 43,
    76, 66,
    76, 68,
    76, 70,
    76, 72,
    76, 80,
    76, 82,
    77, 43,
    79, 43,
    80, 55,
    80, 56,
    80, 57,
    80, 58,
    80, 87,
    80, 88,
    80, 89,
    80, 90,
    81, 55,
    81, 56,
    81, 58,
    81, 87,
    81, 89,
    81, 90,
    82, 43,
    82, 75,
    83, 66,
    83, 68,
    83, 70,
    83, 72,
    83, 80,
    83, 82,
    85, 43,
    86, 43,
    87, 66,
    87, 68,
    87, 70,
    87, 72,
    87, 80,
    87, 82,
    88, 66,
    88, 68,
    88, 70,
    88, 72,
    88, 80,
    88, 82,
    89, 43,
    91, 43,
    92, 43,
    92, 75
};

/* Kerning between the respective left and right glyphs
 * 4.4 format which needs to scaled with `kern_scale`*/
static const int8_t kern_pair_values[] =
{
    13, 10, -36, -5, -5, -36, -8, -5,
    -10, -15, -20, -15, -26, -31, -10, -10,
    -18, -5, -18, -5, 5, -15, -18, -13,
    -15, -5, -5, -5, -5, -5, -5, 5,
    -5, -3, -8, 5, -26, -10, -5, -5,
    -5, -5, -5, -5, 5, 5, 5, 8,
    -10, -10, -10, -10, -10, -10, 3, 8,
    -18, -13, -8, -13, 5, 5, -5, -3,
    -8, -28, -8, -5, -5, -3, -8, 8,
    -3, -3, -3, -3, -3, -3, -3, -31,
    -13, -15, -15, -15, -15, -10, -10, -15,
    -10, -15, -10, -13, -10, -20, -18, -10,
    -10, -10, -8, -5, -5, -10, -5, -10,
    -5, -5, -5, -15, -13, -8, -8, -8,
    -8, -8, -8, -8, -8, -8, -8, -8,
    -8, -5, 8, -8, -8, -8, -8, -8,
    -8, -26, -15, -23, -23, -23, -23, -23,
    -10, -10, -23, -10, -23, -10, -18, -10,
    -10, 8, 13, 10, 13, 10, 5, -8,
    -8, -23, -5, -3, -5, -5, 5, 5,
    -8, -8, -23, -5, -3, -5, -5, 5,
    5, -5, -5, -5, -5, -5, -5, 8,
    3, -10, -8, -8, -23, -5, -3, -5,
    -5, -8, -8, -23, -5, -5, -5, 5,
    10, -5, -5, -5, -5, -5, -5, 5,
    3, -5, -5, -5, -5, -5, -5, -3,
    -3, -3, -3, -3, -3, 5, 5, 13,
    10
};

/*Collect the kern pair's data in one place*/
static const lv_font_fmt_txt_kern_pair_t kern_pairs =
{
    .glyph_ids = kern_pair_glyph_ids,
    .values = kern_pair_values,
    .pair_cnt = 217,
    .glyph_ids_size = 0
};

/*--------------------
 *  ALL CUSTOM DATA
 *--------------------*/

#if LV_VERSION_CHECK(8, 0, 0)
/*Store all the custom data of the font*/
static  lv_font_fmt_txt_glyph_cache_t cache;
static const lv_font_fmt_txt_dsc_t font_dsc = {
#else
static lv_font_fmt_txt_dsc_t font_dsc = {
#endif
    .glyph_bitmap = glyph_bitmap,
    .glyph_dsc = glyph_dsc,
    .cmaps = cmaps,
    .kern_dsc = &kern_pairs,
    .kern_scale = 16,
    .cmap_num = 2,
    .bpp = 1,
    .kern_classes = 0,
    .bitmap_format = 0,
#if LV_VERSION_CHECK(8, 0, 0)
    .cache = &cache
#endif
};


/*-----------------
 *  PUBLIC FONT
 *----------------*/

/*Initialize a public general font descriptor*/
#if LV_VERSION_CHECK(8, 0, 0)
const lv_font_t ui_font_Font1 = {
#else
lv_font_t ui_font_Font1 = {
#endif
    .get_glyph_dsc = lv_font_get_glyph_dsc_fmt_txt,    /*Function pointer to get glyph's data*/
    .get_glyph_bitmap = lv_font_get_bitmap_fmt_txt,    /*Function pointer to get glyph's bitmap*/
    .line_height = 17,          /*The maximum line height required by the font*/
    .base_line = 4,             /*Baseline measured from the bottom of the line*/
#if !(LVGL_VERSION_MAJOR == 6 && LVGL_VERSION_MINOR == 0)
    .subpx = LV_FONT_SUBPX_NONE,
#endif
#if LV_VERSION_CHECK(7, 4, 0) || LVGL_VERSION_MAJOR >= 8
    .underline_position = -2,
    .underline_thickness = 1,
#endif
    .dsc = &font_dsc           /*The custom font data. Will be accessed by `get_glyph_bitmap/dsc` */
};



#endif /*#if UI_FONT_FONT1*/

