/***********************************MQTT 测试说明*******************************************
 *说明：
 * 	 通过MQTT协议连接阿里云, 向阿里云订阅主题和发布温度，湿度消息
 *********************************************************************************************/
#include "mqtt/MQTTClient.h"
#include "system/includes.h"
#include "wifi/wifi_connect.h"
#include "app_config.h"
#include "cJSON.h"
#include "icheima.h"
#include "device/gpio.h"
#ifdef USE_ICHEIMA_HUAWEI_MQTT_DEMO

#define COMMAND_TIMEOUT_MS 30000        //命令超时时间
#define MQTT_TIMEOUT_MS 10000           //接收阻塞时间
#define MQTT_KEEPALIVE_TIME 30000       //心跳时间
#define SEND_BUF_SIZE 1024              //发送buf大小
#define READ_BUF_SIZE 1024              //接收buf大小
static char send_buf[SEND_BUF_SIZE];    //发送buf
static char read_buf[READ_BUF_SIZE];    //接收buf

static int led_state = 1;

void toggle_led(){
    gpio_direction_output(IO_PORTC_00, led_state);
    led_state = !led_state;
}
/*
    修改了设备参数之后,下面代码也要跟着变动
*/
static Client client;

static char payload[512] = "{\"services\": [{\"service_id\": \"BasicData\",\"event_time\":\"20240125T112430Z\",\"properties\": {\"DoorOpenRecord\": %s}}]}";
/* 
static char *subscribeTopic = "$oc/devices/6625d44971d845632a067c21_AF-15-09-4A-4C-2D/sys/commands/#";//订阅的主题
static char *publishTopic = "$oc/devices/6625d44971d845632a067c21_AF-15-09-4A-4C-2D/sys/properties/report";   //发布消息的主题

static char *username = "6625d44971d845632a067c21_AF-15-09-4A-4C-2D";
static char *password = "b3ab2fa05e35b403f418f910e155738c3a6f47dcb9b469c64d9fb6c0c0270769";
static char *clientID = "6625d44971d845632a067c21_AF-15-09-4A-4C-2D_0_0_2024050806";
static char *address = "9febf975a4.st1.iotda-device.cn-north-4.myhuaweicloud.com";

static char *reply_Topic_p = "$oc/devices/6625d44971d845632a067c21_AF-15-09-4A-4C-2D/sys/commands/response/request_id=%s"; //
*/
/*
static char *subscribeTopic = "$oc/devices/6625d44971d845632a067c21_D2-3C-41-01-73-EC/sys/commands/#";//订阅的主题
static char *publishTopic = "$oc/devices/6625d44971d845632a067c21_D2-3C-41-01-73-EC/sys/properties/report";   //发布消息的主题
static char *reply_Topic_p = "$oc/devices/6625d44971d845632a067c21_D2-3C-41-01-73-EC/sys/commands/response/request_id=%s"; 

static char *username = "6625d44971d845632a067c21_D2-3C-41-01-73-EC";
static char *password = "34dd964cb0f4b77705a2963202d4fa877fd96487e9fbb0c579c75a327fe65a4d";
static char *clientID = "6625d44971d845632a067c21_D2-3C-41-01-73-EC_0_0_2024050606";
static char *address = "9febf975a4.st1.iotda-device.cn-north-4.myhuaweicloud.com";
*/
/*
static char *subscribeTopic = "$oc/devices/6625d44971d845632a067c21_F8-18-C4-77-E6-60/sys/commands/#";//订阅的主题
static char *publishTopic = "$oc/devices/6625d44971d845632a067c21_F8-18-C4-77-E6-60/sys/properties/report";   //发布消息的主题
static char *reply_Topic_p = "$oc/devices/6625d44971d845632a067c21_F8-18-C4-77-E6-60/sys/commands/response/request_id=%s"; 

static char *username = "6625d44971d845632a067c21_F8-18-C4-77-E6-60";
static char *password = "7d647fb112a1d890b06fda2f5827871275949d8e76357fdd12e0ee7cd4d0fd0f";
static char *clientID = "6625d44971d845632a067c21_F8-18-C4-77-E6-60_0_0_2024050606";
*/
/*
static char *subscribeTopic = "$oc/devices/6625d44971d845632a067c21_10-8A-9B-8F-EC-3C/sys/commands/#";//订阅的主题
static char *publishTopic = "$oc/devices/6625d44971d845632a067c21_10-8A-9B-8F-EC-3C/sys/properties/report";   //发布消息的主题
static char *reply_Topic_p = "$oc/devices/6625d44971d845632a067c21_10-8A-9B-8F-EC-3C/sys/commands/response/request_id=%s"; 

static char *username = "6625d44971d845632a067c21_10-8A-9B-8F-EC-3C";
static char *password = "ccfbb1479b6db40939a07bdacce7946500f9068170f890892a09be6a30be24d8";
static char *clientID = "6625d44971d845632a067c21_10-8A-9B-8F-EC-3C_0_0_2024050606";
*/
static char *subscribeTopic = "$oc/devices/6625d44971d845632a067c21_FD-72-01-52-FE-E8/sys/commands/#";//订阅的主题
static char *publishTopic = "$oc/devices/6625d44971d845632a067c21_FD-72-01-52-FE-E8/sys/properties/report";   //发布消息的主题
static char *reply_Topic_p = "$oc/devices/6625d44971d845632a067c21_FD-72-01-52-FE-E8/sys/commands/response/request_id=%s"; 

static char *username = "6625d44971d845632a067c21_FD-72-01-52-FE-E8";
static char *password = "a38f6538321a469e669898cc3237f9bed04c2278bc70fcf70c1350b8477227ec";
static char *clientID = "6625d44971d845632a067c21_FD-72-01-52-FE-E8_0_0_2024050606";




static char *address = "9febf975a4.st1.iotda-device.cn-north-4.myhuaweicloud.com";
static char reply_Topic[256];



struct TempLockPassword tempLockPassword;

struct LockMessage {
    int type;
    char userId[30];
    char passwordId[30];
};
// 上传的参数
struct LockMessage lockMessage;

void icheima_mqtt_reply_task()
{
    if(client.isconnected){


        char value[100] = "123";

        MQTTMessage message;
        message.qos = QOS1;
        message.retained = 0;

        message.payload = value;
        message.payloadlen = strlen(value) + 1;

        //发布消息
        int err = MQTTPublish(&client, reply_Topic, &message);
        if (err != 0) {
            printf("MQTTPublish fail, err : 0x%x\n", err);
        }
        printf("MQTTPublish payload:(%s)\n", value);
    }
}


void icheima_mqtt_reply(){

    if (thread_fork("icheima_mqtt_reply_task", 10, 512, 0, NULL, icheima_mqtt_reply_task, NULL) != OS_NO_ERR) {
        printf("icheima_mqtt_reply_task thread fork fail\n");
    }
}


void icheima_mqtt_publish_task(struct LockMessage* msg)
{
    int type = msg->type;
    char* userid = msg->userId;
    char* passwordId = msg->passwordId;

    if(client.isconnected){
        extern void icheima_get_current_time(char time_str[]);

        char time_str[64];

        icheima_get_current_time(time_str);

        char value[100] = "\"{'type':%d,'userId':'%s','openTime': '%s','passwordId':'%s'}\"";


        MQTTMessage message;
        message.qos = QOS1;
        message.retained = 0;

        char sendbuf1[256];
        char sendbuf2[512];
        sprintf(sendbuf1,value,type,userid,time_str,passwordId);
        sprintf(sendbuf2, payload, sendbuf1);
        message.payload = sendbuf2;
        message.payloadlen = strlen(sendbuf2) + 1;


                //发布消息
        int err = MQTTPublish(&client, publishTopic, &message);
        if (err != 0) {
            printf("MQTTPublish fail, err : 0x%x\n", err);
        }

        printf("MQTTPublish payload:(%s)\n", sendbuf2);
    }
}


void icheima_mqtt_publish(int type,char* userId,char* passwordId){

    lockMessage.type = type;

    strcpy(lockMessage.userId,userId);
    strcpy(lockMessage.passwordId,passwordId);

    if (thread_fork("icheima_mqtt_publish_task", 10, 512, 0, NULL, icheima_mqtt_publish_task, &lockMessage) != OS_NO_ERR) {
        printf("icheima_mqtt_publish_task thread fork fail\n");
    }
}


//接收回调，当订阅的主题有信息下发时，在这里接收
static void messageArrived(MessageData *data)
{
    char temp[512] = {0};

    strncpy(temp, data->topicName->lenstring.data, data->topicName->lenstring.len);
    temp[data->topicName->lenstring.len] = '\0';
    printf("Message arrived on topic (len : %d, topic : %s)\n", data->topicName->lenstring.len, temp);

    memset(temp, 0, sizeof(temp));
    strncpy(temp, data->message->payload, data->message->payloadlen);
    temp[data->message->payloadlen] = '\0';
    printf("message (len : %d, payload : %s  id=%s dup=%s)\n", data->message->payloadlen, temp,data->message->retained,data->message->dup);
    // 接收到服务器发送过来的数据
    // {"paras":{"userId":123},"service_id":"default","command_name":"remoteOpen"}
    // {"paras":{"id":123,"userId":456,"deadline":"13241","count":1324,"password":"1341"},"service_id":"default","command_name":"temporary_password"})
	cJSON *monitor_json = cJSON_Parse(temp);
    if(monitor_json == NULL){
		goto end;
	}
	cJSON* command_name = cJSON_GetObjectItem(monitor_json, "command_name");
	if(command_name){
		if(strcmp(command_name->valuestring,"RemoteOpen") == 0){
			 gpio_direction_output(IO_PORTC_00, led_state);
			 led_state = !led_state;

            cJSON* paras = cJSON_GetObjectItem(monitor_json, "paras");
            cJSON* parse_obj = cJSON_GetObjectItem(paras, "userId");
            // 记录远程用户ID
            strcpy(tempLockPassword.userId,parse_obj->valuestring);

			 icheima_mqtt_publish(1,parse_obj->valuestring,"-1");

			 post_msg_play_flash_mp3("004.mp3", 50);
		}else if(strcmp(command_name->valuestring,"TemporaryPassword") == 0){
            post_msg_play_flash_mp3("ring.mp3", 50);

            cJSON* paras = cJSON_GetObjectItem(monitor_json, "paras");

            cJSON* parse_obj = cJSON_GetObjectItem(paras, "id");
            if(parse_obj == NULL){
                goto end;
            }

            strcpy(tempLockPassword.id,parse_obj->valuestring);

            parse_obj = cJSON_GetObjectItem(paras, "password");
            strcpy(tempLockPassword.password,parse_obj->valuestring);

            parse_obj = cJSON_GetObjectItem(paras, "userId");
            strcpy(tempLockPassword.userId,parse_obj->valuestring);

            parse_obj = cJSON_GetObjectItem(paras, "deadline");
            strcpy(tempLockPassword.deadline,parse_obj->valuestring);

            parse_obj = cJSON_GetObjectItem(paras, "count");
            tempLockPassword.count = parse_obj->valueint;


            //extern char temp_lock_password[10];
            //strcpy(temp_lock_password,password_obj->valuestring);

            printf("password:%s    %s\r\n",tempLockPassword.password,tempLockPassword.userId);
		}else{

		}
	}

    char* ptr;
    char* end;
    // 查找 "request_id=" 字符串
    ptr = strstr(data->topicName->lenstring.data, "request_id=");
    if (ptr == NULL) {
        printf("未找到 request_id\n");
        return ;
    }

    // 找到 "=" 之后的字符串
    ptr += strlen("request_id=");

    // 找到下一个 "/" 字符
    end = strchr(ptr, '/');
    if (end != NULL) {
        *end = '\0'; // 将 "/" 替换为 null 终止符
    }
    memset(reply_Topic, 0, sizeof(reply_Topic));
    sprintf(reply_Topic, reply_Topic_p, ptr);
    printf("request_id: %s\n", reply_Topic);

end:
    cJSON_Delete(monitor_json);
    icheima_mqtt_reply();
}






static int mqtt_start(void)
{

    Network network;
    MQTTPacket_connectData connectData = MQTTPacket_connectData_initializer;
    MQTTMessage message;




    strcpy(lockMessage.passwordId,"-1");
    strcpy(lockMessage.userId,"-1");

    int err;
    int loop_cnt = 0;
    int temperature = 0;
    int humidity = 0;

    char sendbuf[256];



_reconnect:
    //初始化网络接口
    NewNetwork(&network);

    SetNetworkRecvTimeout(&network, 1000);

    //初始化客户端
    MQTTClient(&client, &network, COMMAND_TIMEOUT_MS, send_buf, sizeof(send_buf), read_buf, sizeof(read_buf));

    //tcp层连接服务器
    err = ConnectNetwork(&network, address, 1883);
    if (err != 0) {
        printf("ConnectNetwork fail\n");
        return -1;
    }

    connectData.willFlag = 0;
    connectData.MQTTVersion = 3;                                   //mqtt版本号
    connectData.clientID.cstring = clientID;                       //客户端id
    connectData.username.cstring = username;                       //连接时的用户名
    connectData.password.cstring = password;                       //连接时的密码
    connectData.keepAliveInterval = MQTT_KEEPALIVE_TIME / 1000;    //心跳时间
    connectData.cleansession = 1;                                  //是否使能服务器的cleansession，0:禁止, 1:使能

    //mqtt层连接,向服务器发送连接请求
    err = MQTTConnect(&client, &connectData);
    if (err != 0) {
        network.disconnect(&network);
        printf("MQTTConnect fail, err : 0x%x\n", err);
        return -1;
    }

    //订阅主题
    err = MQTTSubscribe(&client, subscribeTopic, QOS1, messageArrived);
    if (err != 0) {
        MQTTDisconnect(&client);
        network.disconnect(&network);
        printf("MQTTSubscribe fail, err : 0x%x\n", err);
        return -1;
    }

    //取消主题订阅
    //MQTTUnsubscribe(&client, subscribeTopic);

    message.qos = QOS1;
    message.retained = 0;

    while (1) {
        if (0 == loop_cnt % 2) {
            //icheima_mqtt_publish(3,123,456);
            //icheima_mqtt_publish(1,123,456);
            /*
            sprintf(sendbuf, payload, value);
            message.payload = sendbuf;
            message.payloadlen = strlen(sendbuf) + 1;


            //发布消息
            err = MQTTPublish(&client, publishTopic, &message);
            if (err != 0) {
                printf("MQTTPublish fail, err : 0x%x\n", err);
            }

            printf("MQTTPublish payload:(%s)\n", sendbuf);
            */
        }

        loop_cnt += 1;

        err = MQTTYield(&client, MQTT_TIMEOUT_MS);
        if (err != 0) {
            if (client.isconnected) {
                //断开mqtt层连接
                err = MQTTDisconnect(&client);
                if (err != 0) {
                    printf("MQTTDisconnect fail\n");
                }

                //断开tcp层连接
                network.disconnect(&network);
            }

            printf("MQTT : Reconnecting\n");

            //重新连接
            goto _reconnect;
        }

    }

    return 0;
}

static void aliyun_mqtt_example(void)
{
    if (thread_fork("mqtt_start", 10, 2 * 1024, 0, NULL, mqtt_start, NULL) != OS_NO_ERR) {
        printf("thread fork fail\n");
    }
}
    #include "le_trans_data.h"
#include "le_common.h"
static void mqtt_test(void *priv)
{
    int err;
    enum wifi_sta_connect_state state;
    os_time_dly(1000);
    //wifi_enter_sta_mode("icheima", "abcdefgh");
    //wifi_sta_connect("icheima", "abcdefgh",1);

    while (1) {
        printf("huawei Connecting to the network...\n");
        state = wifi_get_sta_connect_state();
        toggle_led();
        if (WIFI_STA_NETWORK_STACK_DHCP_SUCC == state) {
            printf("Network connection is successful!\n");
            break;
        }

        os_time_dly(500);
    }
    // post_msg_play_flash_mp3("NetCfgSucc.mp3", 50);

/*
    #if TCFG_USER_BLE_ENABLE
        char* temp_data = "{\"status\":200,\"msg\":\"config success\",\"command\":\"network\"}";
        extern int app_send_user_data(u16 handle, const u8 *data, u16 len, u8 handle_type);            // 接收参数成功
        app_send_user_data(ATT_CHARACTERISTIC_ae04_01_VALUE_HANDLE, temp_data, strlen(temp_data), ATT_OP_NOTIFY);
    #endif
*/

    gpio_direction_output(IO_PORTC_00, 1);
    aliyun_mqtt_example();
}

//应用程序入口,需要运行在STA模式下
int c_main(void)
{
    if (thread_fork("mqtt_test", 10, 512, 0, NULL, mqtt_test, NULL) != OS_NO_ERR) {
        printf("thread fork fail\n");
    }

    return 0;
}

late_initcall(c_main);

#endif//USE_MQTT_TEST
