#include "app_config.h"

#if LV_USE_PERF_MONITOR || LV_USE_MEM_MONITOR
#include "widgets/lv_label.h"
#endif

#ifdef USE_LVGL_UI_DEMO
#include <time.h>
#include <sys/time.h>
#include "lv_conf.h"
#include "system/includes.h"
#include "lvgl.h"
//#include "lv_demo_benchmark.h"
#include "lv_demo_widgets.h"
//#include "lv_example_freetype.h"
#include "lv_port_disp.h"
#include "lv_port_indev.h"
#include "lv_port_fs.h"
#include "smart_watch/ui.h"

static void lvgl_fs_test(void)
{
    extern int storage_device_ready(void);
    while (!storage_device_ready()) {//等待sd文件系统挂载完成
        os_time_dly(5);
        puts("lvgl waitting sd on... ");
    }

#if 1
    lv_obj_t *obpng = lv_img_create(lv_scr_act());
    lv_img_set_src(obpng, CONFIG_ROOT_PATH"icon.png");
    lv_obj_align(obpng, LV_ALIGN_CENTER, 0, 0);
#else
    lv_obj_t *imgbtn = lv_imgbtn_create(lv_scr_act());
    lv_imgbtn_set_src(imgbtn, LV_IMGBTN_STATE_PRESSED, NULL, CONFIG_ROOT_PATH"press_icon.bin", NULL);
    lv_imgbtn_set_src(imgbtn, LV_IMGBTN_STATE_RELEASED, NULL, CONFIG_ROOT_PATH"release_icon.bin", NULL);
    lv_obj_align(imgbtn, LV_ALIGN_CENTER, 0, 0);
#endif
}

static OS_MUTEX mutex;
static void lvgl_time_ui_update(){
    char time_str[64];
    struct tm timeinfo;
    time_t timestamp;

    char buffer[10];
    char temp="%02d   %02d"; // "%02d   %02d"

    while(1){
        timestamp = time(NULL) + 28800;
        localtime_r(&timestamp, &timeinfo);
        strftime(time_str, sizeof(time_str), "%Y-%m-%dT%H:%M:%S", &timeinfo);
        //printf("RTC TIME [%s]\n", time_str);

        /* timeinfo.tm_hour  timeinfo.tm_min timeinfo.tm_sec
            ui_time   lv_label_set_text(ui_time, "03   27");
            ui_sec    lv_img_set_angle(ui_sec, 800); // 3600   1.0*20/60*3600
            ui_min    lv_img_set_angle(ui_sec, 800);
            ui_hour   lv_img_set_angle(ui_sec, 800);

            static OS_MUTEX mutex;
            os_mutex_pend(&mutex, 0);
            os_mutex_post(&mutex);
            os_mutex_create(&mutex);
        */

        os_mutex_pend(&mutex, 0);

        //sprintf(buffer, temp, timeinfo.tm_hour, timeinfo.tm_min);
        //lv_label_set_text(ui_time, buffer);
        lv_label_set_text_fmt(ui_time,"%02d   %02d",timeinfo.tm_hour, timeinfo.tm_min);

        lv_img_set_angle(ui_sec, 1.0*timeinfo.tm_sec/60*3600);
        lv_img_set_angle(ui_min, 1.0*timeinfo.tm_min/60*3600);
        lv_img_set_angle(ui_hour, 1.0*timeinfo.tm_hour/12*3600);

        os_mutex_post(&mutex);

        os_time_dly(100); // CV大师
    }
}


static void lvgl_main_task(void *priv)
{
    lv_init();
    lv_port_disp_init();
    lv_port_indev_init();
    lv_port_fs_init();
    lv_png_init();

    // uncomment one of these demos
    //    lv_example_arc_1();
    /*lv_demo_benchmark();*/
    /* lv_example_freetype_1(); */
    //lv_demo_widgets();
    //lv_example_rlottie_1();//FIXME:有死机问题 void renderer::Layer::render(VPainter *painter, const VRle &inheritMask
    //lv_example_rlottie_2();
    /* lv_example_btn_1(); */
    /*lv_demo_keypad_encoder();*/
    /*lv_demo_music();          */
    /*lv_demo_stress();           */
    /*lvgl_fs_test();*/
    ui_init();

    os_mutex_create(&mutex);

    thread_fork("lvgl_time_ui_update", 10, 512, 0, NULL, lvgl_time_ui_update, NULL);



    while (1) {
        os_mutex_pend(&mutex, 0);

        u32 time_till_next = lv_timer_handler();

        if (LV_DISP_DEF_REFR_PERIOD > 1 && time_till_next >= 1000 / OS_TICKS_PER_SEC) {
            msleep(time_till_next);
        }

        os_mutex_post(&mutex);
    }
}

static int lvgl_main_task_init(void)
{
    puts("lvgl_main_task_init \n\n");
    //说明由于LVGL没有加延时让出CPU需要配置为最低优先级 高优先级不能长时间占用CPU不然LVGL运行卡顿
    return thread_fork("lvgl_main_task", 1, 8 * 1024, 0, 0, lvgl_main_task, NULL);
}
late_initcall(lvgl_main_task_init);


#endif


